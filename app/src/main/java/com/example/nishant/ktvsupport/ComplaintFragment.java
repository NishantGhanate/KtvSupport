package com.example.nishant.ktvsupport;

import android.content.Context;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Nishant on 12/4/2017.
 */

public class ComplaintFragment extends Fragment {

    Map<Object, Object> docData = new HashMap<>();
    Map<Object, Object> nestedData = new HashMap<>();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String username = user.getDisplayName();
    String emailid = user.getEmail();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    DocumentReference docRefComplaint ;   // you can also use [Complaint/Complaints]
    CollectionReference collectionReference;
    String TAG = "Complaint Fragment";
    Button buttonReport;
    EditText editTextComplaint;
    String complaintInputData;
    Context context;
    String provider = user.getProviders().get(0); // This will give info about user sign in selection
    String google = "google.com";
    String phone ="phone";
    String doc = null;
    Toast toast;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       // return super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.complaint_fragment,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       editTextComplaint = (EditText)view.findViewById(R.id.editTextComplaint);
       buttonReport = (Button)view.findViewById(R.id.buttonReport);
       buttonReport.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Call_complaint();
           }
       });

        super.onViewCreated(view, savedInstanceState);
    }

    private void Call_complaint()
    {
        String time = new Date().toString();
        complaintInputData = editTextComplaint.getText().toString().trim(); // [ Get user data ]
        if(TextUtils.isEmpty(complaintInputData)){
            return;
        }
        nestedData.put("Time",time);
        nestedData.put("Complaint", complaintInputData);                    //[ put that into object ]
        nestedData.put("Solved","");

       // docData.put(time, nestedData);
        // Add data to database
        final Random random = new Random();
             String rand = random.toString();

        if ((google).equals(provider)) {
            doc=user.getEmail();
        }
        else{
            doc=user.getPhoneNumber();
        }


        docRefComplaint  = db.collection("Users").document(doc).collection("Complaints").document(time); //Setting up path
        docRefComplaint.set(nestedData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                toast = Toast.makeText(getActivity().getApplicationContext(), "Your complaint has been successfully registered", Toast.LENGTH_SHORT );
                toast.show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                toast = Toast.makeText(getActivity().getApplicationContext(), "Something went wrong ", Toast.LENGTH_SHORT );
                toast.show();

            }
        });


        editTextComplaint.setText("");
        complaintInputData=null;


    }// [End of function ]


} //[ END ] ----------------------------------------------------------------------------------------
