package com.example.nishant.ktvsupport;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import static java.security.AccessController.getContext;

public class MainActivity extends AppCompatActivity {
private Button buttonGoogleSign,buttonPhoneLogin;
private ProgressDialog progressDialog;
private FirebaseAuth mAuth;
private FirebaseAuth.AuthStateListener mAuthListner;
private GoogleSignInClient mGoogleSignInClient;
private Animation uptodown,downtoup;
private static final int RC_SIGN_IN = 123; //Request code
    private static final String TAG = "GoogleActivity";
    String answer;
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListner); //As app launches initialize auth listener
        uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        buttonGoogleSign.setAnimation(uptodown); // set animation from top to down
        buttonPhoneLogin.setAnimation(downtoup); // Set animation from down to top
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        getSupportActionBar().hide();
        buttonGoogleSign = (Button)findViewById(R.id.buttonGoogleLogin);
        buttonPhoneLogin = (Button)findViewById(R.id.buttonPhoneLogin);
        progressDialog = new ProgressDialog(this);

        buttonPhoneLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( getApplicationContext() , PhoneSignInActivity.class));

            }
        });
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

        if (activeNetwork == null) {
            if (activeNetwork.getType() != ConnectivityManager.TYPE_WIFI)
                answer = "You are not connected to a WiFi Network";
            if (activeNetwork.getType() != ConnectivityManager.TYPE_MOBILE)
                answer = "You are not connected to a Mobile Network";
            Toast.makeText(getApplicationContext(), answer, Toast.LENGTH_LONG).show();
            return;
        }


        mAuth = FirebaseAuth.getInstance();   // [START initialize_auth get Instance]

        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if(firebaseAuth.getCurrentUser()!= null){   // Check  if user is logged in
                    startActivity(new Intent( getApplicationContext() , UserActivity.class)); // Start new activity and end the current one
                    finish();
                }
            }
        }; // Auth state listener


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN) // Configure Google Sign In
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        buttonGoogleSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Google_signin();
            }
        });

    }// End of onCreate

    private void Google_signin(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                GoogleSignInAccount account = task.getResult(ApiException.class);   // Google Sign In was successful, authenticate with Firebase
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);  // Google Sign In failed, update UI appropriately
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {  // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                           // FirebaseUser user = mAuth.getCurrentUser();

                            startActivity(new Intent( getApplicationContext() , UserActivity.class));
                            finish();
                        }
                        else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }); // End of Sign on Listener
    }



}// End of Class
