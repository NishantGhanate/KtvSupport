package com.example.nishant.ktvsupport;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Nishant on 14-Dec-17.
 */

class FixAdapter extends RecyclerView.Adapter<FixAdapter.HolderView> {
    List<FixList> Fix = new ArrayList<>();
    public FixAdapter(List<FixList> fix) {
        this.Fix = fix;
    }


    @Override
    public FixAdapter.HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
         View Layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.fixproblem_cardview,parent,false);
         return new HolderView(Layout);
    }

    @Override
    public void onBindViewHolder(FixAdapter.HolderView holder, int position) {
       // holder.textViewF.setText(Fix.get(position).getA1());
    }

    @Override
    public int getItemCount() {
        return Fix.size();
    }

    public class HolderView extends RecyclerView.ViewHolder {
        TextView textViewF;
        public HolderView(View itemView) {
            super(itemView);
            //textViewF = (TextView)itemView.findViewById(R.id.textViewF);
        }
    }
}
