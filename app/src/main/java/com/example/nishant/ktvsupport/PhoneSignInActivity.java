package com.example.nishant.ktvsupport;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;



public class PhoneSignInActivity extends AppCompatActivity {
private Button buttonSendOTP,buttonVerifyOTP;
private EditText editTextPhoneNumber , editTextOTP;
    private static final String TAG = "PhoneLogin";
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private Animation Left_to_right,Right_to_left;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_sign_in);
      //  getSupportActionBar().hide();
        Left_to_right  = AnimationUtils.loadAnimation(this,R.anim.left_to_right);
        Right_to_left  = AnimationUtils.loadAnimation(this,R.anim.right_to_left);
        editTextPhoneNumber = (EditText)findViewById(R.id.editTextPhoneNumber);
        editTextOTP = (EditText)findViewById(R.id.editTextOTP);
        buttonSendOTP = (Button)findViewById(R.id.buttonSendOTP);
        buttonVerifyOTP = (Button)findViewById(R.id.buttonVerifyOTP);

        editTextPhoneNumber.setAnimation(Left_to_right); // SET ANIMATION FROM LEFT TO RIGHT
        editTextOTP.setAnimation(Left_to_right);

        buttonSendOTP.setAnimation(Right_to_left);       // SET ANIMATION FROM RIGHT TO LEFT
        buttonVerifyOTP.setAnimation(Right_to_left);


        mAuth = FirebaseAuth.getInstance(); // Initialize FIREBASE AUTH

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted:" + phoneAuthCredential);
                mVerificationInProgress = false;
                Toast.makeText(PhoneSignInActivity.this,"Verification Complete",Toast.LENGTH_SHORT).show();
                signInWithPhoneAuthCredential(phoneAuthCredential); // pass phone details to sign in function
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                Toast.makeText(PhoneSignInActivity.this,"Verification Failed",Toast.LENGTH_SHORT).show();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    Toast.makeText(PhoneSignInActivity.this, "InValid Phone Number", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;       // Save verification ID and resending token so we can use them later
                mResendToken = token;
            }
        };

        buttonSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = editTextPhoneNumber.getText().toString().trim();
                if(TextUtils.isEmpty(phoneNumber)){
                    Toast.makeText(PhoneSignInActivity.this, "Please enter your phone number", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(PhoneSignInActivity.this, "OTP Sent", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "phoneNumber = " + phoneNumber);
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phoneNumber,
                        60,
                        java.util.concurrent.TimeUnit.SECONDS,
                        PhoneSignInActivity.this,
                        mCallbacks);

                }
        });     // call Back function Button

        buttonVerifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String OTP =  editTextOTP.getText().toString();
                if(TextUtils.isEmpty(OTP)){
                    Toast.makeText(PhoneSignInActivity.this, "Please enter the OPT you received", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.d(TAG, "OTP = " + OTP);
                PhoneAuthCredential phonecredential = PhoneAuthProvider.getCredential(mVerificationId, OTP); // This can cause to crash bc
                // [END verify_with_code]
                signInWithPhoneAuthCredential(phonecredential);
            }

        });// button verify



    }// End of onCreate

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                             Log.d(TAG, "signInWithCredential:success");
                            // startActivity(new Intent(PhoneLogin.this,Home.class));
                            Toast.makeText(PhoneSignInActivity.this,"Verification Done",Toast.LENGTH_SHORT).show();
                            //FirebaseUser user = task.getResult().getUser();
                            Toast.makeText(PhoneSignInActivity.this, " Successful", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent( PhoneSignInActivity.this , UserActivity.class));
                            finish();

                        }
                        else {
                            // Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(PhoneSignInActivity.this,"Invalid Verification",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }


}// End of class


