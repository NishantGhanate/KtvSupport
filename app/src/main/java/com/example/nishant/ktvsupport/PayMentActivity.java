package com.example.nishant.ktvsupport;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class PayMentActivity extends AppCompatActivity {


    TextView textViewSpeedPay,textViewValidPay,textViewPricePay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_ment);
        textViewSpeedPay = findViewById(R.id.textViewSpeedPay);
        textViewValidPay = findViewById(R.id.textViewValidPay);
        textViewPricePay = findViewById(R.id.textViewPricePay);
        String speed = getIntent().getStringExtra("speed");
        String valid = getIntent().getStringExtra("valid");
        String price = getIntent().getStringExtra("price");
        if (price != null) { //The key argument here must match that used in the other activity
            textViewPricePay.setText(speed);
            textViewValidPay.setText(valid);
            textViewSpeedPay.setText(price);
        }

    }
}
