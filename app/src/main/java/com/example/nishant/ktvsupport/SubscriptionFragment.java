package com.example.nishant.ktvsupport;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Nishant on 12/3/2017.
 *
 * extend Fragment android.v4.app
 */

public class SubscriptionFragment extends Fragment  {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String username = user.getDisplayName();
    String emailid = user.getEmail();
    CollectionReference collectionReference;
    DocumentReference docInternetplan ;   // you can also use [Complaint/Complaints]
    String provider = user.getProviders().get(0); // This will give info about user sign in selection
    String google = "google.com";
    String phone ="phone";
    String doc = null;
    String TAG = "Feed Back Fragment";
    Toast toast;
    TextView textView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       // return super.onCreateView(inflater, container, savedInstanceState);
       return inflater.inflate(R.layout.subscription_fragment,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textView = (TextView)view.findViewById(R.id.textViewPlanSpeed);
        ReadPlan();

    }

    private void ReadPlan() {
        if ((google).equals(provider)) {
            doc=user.getEmail();
        }
        else{
            doc=user.getPhoneNumber();
        }
        docInternetplan  = db.collection("Users").document(doc).collection("Complaints").document("InternetPLan"); //Setting up path


    }//[End of Read  ]


}

