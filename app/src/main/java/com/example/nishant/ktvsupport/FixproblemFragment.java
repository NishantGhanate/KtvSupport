package com.example.nishant.ktvsupport;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nishant on 12/4/2017.
 */

public class FixproblemFragment extends Fragment implements SearchView.OnQueryTextListener {
    SearchView searchView;
    RecyclerView recyclerViewFix;
    FixAdapter adapter;
    List<FixList> Fix = new ArrayList<>();
    Context context;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fixproblem_fragment,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerViewFix = (RecyclerView)view.findViewById(R.id.recycleView_Fix);
        Fix.add(new FixList("a","s","d"));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerViewFix.setLayoutManager(linearLayoutManager);
        adapter = new FixAdapter(Fix);
        recyclerViewFix.setAdapter(adapter);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_view, menu);
        final MenuItem search_bar = menu.findItem(R.id.search_bar); // Initializing search bar
        searchView = (SearchView) search_bar.getActionView();
        searchView.setOnQueryTextListener(this);
        return  ;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {


        return false;
    }
}
