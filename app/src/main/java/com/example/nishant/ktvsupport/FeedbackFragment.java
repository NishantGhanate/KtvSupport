package com.example.nishant.ktvsupport;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nishant on 12/4/2017.
 */

public class FeedbackFragment extends Fragment {
    EditText editTextFeedBack;
    Button buttonSend;
    String inputFeedBack;
    Map<Object, Object> nestedData = new HashMap<>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    CollectionReference collectionReference;
    DocumentReference docRefFeedBack ;   // you can also use [Complaint/Complaints]
    String provider = user.getProviders().get(0); // This will give info about user sign in selection
    String google = "google.com";
    String phone ="phone";
    String doc = null;
    String TAG = "Feed Back Fragment";
    Toast toast;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.feedback_fragment,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTextFeedBack = (EditText)view.findViewById(R.id.editTextFeedBack);
        buttonSend = (Button)view.findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Send_Fragment();
            }
        });


    }

    private void Send_Fragment() {
        String time = new Date().toString();
        inputFeedBack = editTextFeedBack.getText().toString().trim();
        if(TextUtils.isEmpty(inputFeedBack)){
            return;
        }
        nestedData.put("Time",time);
        nestedData.put("Feed Back", inputFeedBack);                    //[ put that into object ]

        if ((google).equals(provider)) {
            doc=user.getEmail();
        }
        else{
            doc = user.getPhoneNumber();
        }

        docRefFeedBack  = db.collection("Users").document(doc).collection("Feed Back").document(time); //Setting up path
        docRefFeedBack.set(nestedData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                toast = Toast.makeText(getActivity().getApplicationContext(), "Your feedback have been successfully registered", Toast.LENGTH_SHORT );
                toast.show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                toast = Toast.makeText(getActivity().getApplicationContext(), "Something went wrong ", Toast.LENGTH_SHORT );
                toast.show();

            }
        });

        editTextFeedBack.setText("");
        inputFeedBack =null;
    }


} //[ END ]
