package com.example.nishant.ktvsupport;

import android.content.Context;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Nishant on 12/10/2017.
 */

public class InternetPlan_Fragment extends Fragment implements SearchView.OnQueryTextListener {
    Context context;
    InternetPlanAdapter adapter;
    RecyclerView recyclerView;  //Declaration
    SearchView searchView;
    List<Item> productlists = new ArrayList<>();  //Create class Item to handle cardView Data
    List<String> speed = new ArrayList<>();
    List<String> validity = new ArrayList<>();
    List<String> price = new ArrayList<>();
    String TAG ="Internet Fragment";
    Bundle bundle ;
     List<Integer> images = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       // return super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.internet_plan_fragment,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = this.getArguments();
        Log.d(TAG," GET DATA ");

        if(bundle!=null){
            speed =  bundle.getStringArrayList("speed");
            validity =  bundle.getStringArrayList("valid");
            price = bundle.getStringArrayList("price");
            if(price==null){
                Toast.makeText(getActivity().getApplicationContext(), " Please restart the app", Toast.LENGTH_SHORT).show();
            }
            else{
                Log.d(TAG," Speed " + speed + " Valid " + validity + " price " + price );
                for(int i = 0 ; i<speed.size(); i++){
                    productlists.add(new Item(speed.get(i),validity.get(i),price.get(i)));     //Adding data in to card
                }
            }
        }

        AddBG();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleView_InternetPlan);
        recyclerView.hasFixedSize();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context); // context in Fragment and this in activity class
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new InternetPlanAdapter(getActivity().getApplicationContext(),productlists,images);
        recyclerView.setAdapter(adapter);

    } //[----------------------------------------------End of on onView---------------------------]

    private void AddBG(){

        images.add(R.drawable.internet_plan_card);
        images.add(R.drawable.internet_plan_card1);
        images.add(R.drawable.internet_plan_card2);
        images.add(R.drawable.internet_plan_card3);
        images.add(R.drawable.internet_plancard4);
        images.add(R.drawable.internet_plancard5);


}



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.search_view, menu);
        inflater.inflate(R.menu.sort,menu);
        final MenuItem search_bar = menu.findItem(R.id.search_bar); // Initializing search bar
        searchView = (SearchView) search_bar.getActionView();
        searchView.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        newText = newText.toLowerCase();   // newText contains user Query
        List<Item>  newlist = new ArrayList<>(); // make new  object of class Item where data is stored
        String title,titleSpeed,titleValidity;
        for(Item itemList : productlists ){         //Declare Item class object name and loop till end
            title = itemList.getPrice().toLowerCase();  //.getName(String) from Item class and store into title
            titleSpeed = itemList.getPlanspeed().toLowerCase();
            titleValidity = itemList.getValidity().toLowerCase();

            if(title.contains(newText)){      // if user query matched from dataset update the Item class list
                newlist.add(itemList);
            }
            else if(titleSpeed.contains(newText)){
                newlist.add(itemList);
            }
            else if( titleValidity.contains(newText) ){
                newlist.add(itemList);
            }
        }
        adapter.setfilter(newlist);   //Update adapter class method.setFilter
        return true;
    }


} //[ -------------------------------------------------END---------------------------------------- ]

