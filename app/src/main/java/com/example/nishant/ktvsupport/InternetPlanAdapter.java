package com.example.nishant.ktvsupport;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Nishant on 12/10/2017.
 */

public class InternetPlanAdapter extends RecyclerView.Adapter<InternetPlanAdapter.Holderview> {

    private List<Item> productlist = new ArrayList<>();  //Create list from Item class
    List<Integer>ImageViews;
     Context context;
    public InternetPlanAdapter(Context context,List<Item> productlists, List<Integer>ImageViews) {
        this.productlist = productlists;
        this.context = context;
        this.ImageViews = ImageViews;
    }

    @Override
    public Holderview onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.internetplan_cardview,parent,false);

        return new Holderview(layout);
    }

    @Override
    public void onBindViewHolder(Holderview holder,  int position) {
        holder.textViewPrice.setText( productlist.get(position).getPrice());
        holder.textViewPlanSpeed.setText( productlist.get(position).getPlanspeed());
        holder.textViewValidity.setText( productlist.get(position).getValidity());
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        holder.constraintLayout.setBackgroundResource(ImageViews.get(position));
       // holder.imageView.setImageResource(ImageViews.get(position));
    }

    @Override
    public int getItemCount() {
        return productlist.size();
    }

    public void setfilter(List<Item> listitem)
    {
        productlist = new ArrayList<>();
        productlist.addAll(listitem);
        notifyDataSetChanged();

    }

    public class Holderview extends RecyclerView.ViewHolder {
        TextView textViewPrice, textViewPlanSpeed, textViewValidity;
        Button buttonRecharge;
        Context context;
        Intent intent;
        View view;
        CardView cardView;
        Activity activity;
        ImageView imageView;
        ConstraintLayout constraintLayout;

        public Holderview(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card);
            constraintLayout = itemView.findViewById(R.id.cardL);

            textViewPrice = (TextView) itemView.findViewById(R.id.textViewPrice);
            textViewPlanSpeed = (TextView) itemView.findViewById(R.id.textViewPlanSpeed);
            textViewValidity = (TextView) itemView.findViewById(R.id.textViewValidity);

            buttonRecharge = (Button)itemView.findViewById(R.id.buttonRecharge);
            buttonRecharge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String speed = productlist.get(getAdapterPosition()).getPlanspeed();
                    String valid = productlist.get(getAdapterPosition()).getValidity();
                    String price = productlist.get(getAdapterPosition()).getPrice();
                    Toast.makeText(v.getContext(), " Speed " + speed + "\nValidity = " + valid + "\nPrice " + price, Toast.LENGTH_SHORT).show();
                    intent = new Intent(v.getContext(), PayMentActivity.class);
                    intent.putExtra("speed", speed );
                    intent.putExtra("valid", valid );
                    intent.putExtra("price", price );
                    v.getContext().startActivity(intent);


                }
            });

        }

    } //[ End of Holderview Class ]




} //[End of base class]
