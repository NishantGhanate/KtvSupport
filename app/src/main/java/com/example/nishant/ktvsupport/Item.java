package com.example.nishant.ktvsupport;

/**
 * Created by Nishant on 12/10/2017.
 */

public class Item  {
    private String price,planspeed,validity;


    public Item(String planspeed, String validity, String price) {
        this.price = price;
        this.planspeed = planspeed;
        this.validity = validity;


    }

    public String getPrice() {
        return price;                            //This will return Price
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPlanspeed() {
        return planspeed;                            //This will return Price
    }

    public void setPlanspeed(String planspeed) {
        this.planspeed = planspeed;
    }

    public String getValidity() {
        return validity;                            //This will return Price
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }





}
