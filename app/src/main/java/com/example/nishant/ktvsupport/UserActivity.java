package com.example.nishant.ktvsupport;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;



//FirebaseAuth.getInstance().signOut();
public class UserActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

     ImageView imageView;
     FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String provider = user.getProviders().get(0); // This will give info about user sign in selection

    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListner;
    private Map<String, Object> Guser = new HashMap<>();
    private static final String TAG ="Profile Activity" ;

     ArrayList<String> Speed = new ArrayList<>();
     ArrayList<String> Validity = new ArrayList<>();
     ArrayList<String> Price = new ArrayList<>();

    String key;
    Iterator<?> keys;
    String s,v,p;
    JSONObject jsonObject ;


    FirebaseFirestore db = FirebaseFirestore.getInstance();
    DocumentReference docRefPlans = db.collection("Office").document("Internet plans");
    TextView textViewUsername,textViewEmail ;

    DocumentReference UserDocumentRef ;
    Map<String, Object> UserdocData = new HashMap<>();

    FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()  //[For offline ]
            .setPersistenceEnabled(true)
            .build();

    Bundle bundle = new Bundle();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        GetPlans();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_subscription); //select first fragment
       // navigationView.getMenu().performIdentifierAction(R.id.nav_subscription, 0);

        if (savedInstanceState == null){
            this.onNavigationItemSelected(navigationView.getMenu().getItem(0)); //start first fragment
        }

        View header = navigationView.getHeaderView(0); //Nav HEADER CONNECTION

        textViewUsername = (TextView)header.findViewById(R.id.textViewUSerName);
        textViewEmail = (TextView)header.findViewById(R.id.textViewEmailId);
        imageView = (ImageView)header.findViewById(R.id.imageViewDp);

        String google = "google.com";
        String phoneUser = "Phone User";
        String username = user.getDisplayName();
        String doc = null;
        db.setFirestoreSettings(settings);                           //[For offline ]

        if (user != null && provider != null) {
            String email = user.getEmail();
            String phoneNumber = user.getPhoneNumber();
            if ((google).equals(provider)) {
                textViewUsername.setText(username);
                textViewEmail.setText(email);
                Uri photoUrl = user.getPhotoUrl();
                Log.d(TAG, "Logged in from Gmail");
                doc=email;
                try {

                    Picasso.with(this).load(photoUrl).transform(new CircleTransform()).into(imageView); // Call Class to transform
                }catch (Exception e) {
                    e.printStackTrace();
                    }
                }
                else{
                doc = phoneNumber;
                textViewUsername.setText(phoneUser);
                textViewEmail.setText(phoneNumber);
                Log.d(TAG, "Logged in from phone number");
            }
            UserdocData.put("User Name : ", username);
            UserdocData.put("User Email : ", email);
            UserdocData.put("User phone : ", phoneNumber);
            UserDocumentRef = db.collection("Users").document(doc); // Setting up user Database information;
            UserDocumentRef.set(UserdocData).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "UserInfo added Successfully : " );
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w(TAG, "Error adding document", e);
                }
            });

        } // End of user data

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN) // Configure Google Sign In
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
         //Revoke




    }  //-------------------------------- End of onCreate-------------------------------------------



    private void GetPlans(){

        docRefPlans.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {

                        Log.d(TAG, "DocumentSnapshot data Read: " + task.getResult().getData());
                        jsonObject = new JSONObject(task.getResult().getData());
                        keys = jsonObject.keys(); //Storing node count
                        while(keys.hasNext() ) {
                            key = (String) keys.next();
                            Log.d("Node Key","keys =" + key );
                            try {
                                if ( jsonObject.get(key) instanceof JSONObject ){
                                    JSONObject jso = new JSONObject(jsonObject.get(key).toString());
                                    s=jso.getString("Speed");
                                    Speed.add(s);
                                    v = jso.getString("Valid");
                                    Validity.add(v);
                                    p = jso.getString("Price");
                                    Price.add(p);

                                }
                            } catch (JSONException e){e.printStackTrace();}
                        } // while over

                        Log.d(TAG,"Speed =  " + Speed + " Validity =  " + Validity + " Price = " + Price );
                        try{

                            bundle.putStringArrayList("speed",Speed);
                            bundle.putStringArrayList("valid",Validity);
                            bundle.putStringArrayList("price",Price);


                        }
                        catch (Exception e){e.printStackTrace();}

                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });


    } //[ ------------------------------------------End of Get Plans-------------------------------]



    private void HideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

    } //[----------------------------------------------End of HideKeyboard-------------------------]

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signout) {
            signOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    } //[------------------------------------------- End of AppBar Menu----------------------------]

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;

        int id = item.getItemId();   // Handle navigation view item clicks here.
        switch (id){
            case R.id.nav_subscription:{

                HideKeyboard();
                fragment = new SubscriptionFragment();
                break;
            }
            case R.id.nav_InternetPlans :{

                HideKeyboard();
                fragment = new InternetPlan_Fragment();
                fragment.setArguments(bundle);
                break;
            }
            case R.id.nav_complaint:{

                HideKeyboard();
                fragment = new ComplaintFragment();
                break;
            }

            case R.id.nav_fix:{

                HideKeyboard();
                fragment = new FixproblemFragment();
                break;
            }

            case R.id.nav_feedback:{

                HideKeyboard();
                fragment = new FeedbackFragment();
                break;
            }
            case R.id.nav_aboutApp:{

                HideKeyboard();
                fragment = new AboutApp_Fragment();
                break;
            }

        }

        if(fragment!=null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left,
                        android.R.anim.slide_out_right);
            fragmentTransaction.replace(R.id.screenArea,fragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    } //[-------------------------------------------End of Nav-------------------------------------]


    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent( UserActivity.this , MainActivity.class));
                        finish();
                    }
                });
    } //[----------------------------------------------------End of Sign out-----------------------]





} //[----------------------------------------End of class----------------------------------------- ]
